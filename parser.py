import requests
from typing import List
import json
import re
from collections import defaultdict
import html
from datetime import datetime
import shutil


misspellings = {
    "I Only Have Three But:": "",
    "Zeal And Ardor": "Zeal & Ardor",
    "Only 3 This Time": "",
    "Thy Catefalque": "Thy Catafalque",
    "Just Those Three": "",
    "Uhh": "",
    "Panopoticon": "Panopticon",
    "Rivers Of Nile": "Rivers Of Nihil"
}


def clean_comments(comments: List[dict]) -> List[dict]:
    # Remove the mod voting, because those guys' formatting fucks up everything
    comments = [c for c in comments if c['data']['id'] != 'e15sjpw']
    return comments


def remove_double_line_breaks(s: str) -> str:
    while s.find("\n\n") != -1:
        s = s.replace("\n\n", "\n")
    return s


def clean_up_line(l: str) -> str:
    l = html.unescape(l)
    # Trim for good measure
    l = l.strip()
    # Remove leading control characters
    if l.startswith("-"):
        l = l[1:]
    if l.startswith("*"):
        l = l[1:]

    # I guess i need regex for ordered lists
    l = re.sub("^\\d*\\.\\s*", "", l)

    # All useless chars should be
    #  removed by now
    # Split lines in case anyone added the album title
    l = l.split("-")[0]
    l = l.split("–")[0]

    # Titalize each name
    l = l.title()

    # Trim again, because we prolly left whitespace in there along the way
    l = l.strip()

    # remove trailing comma
    if l.endswith(","):
        l = l[:-1]

    if l in misspellings:
        return misspellings[l]

    return l


def is_band(l: str) -> bool:
    # I don't think there is a band with more than 6 words in here, so all of those are propably fluff comments
    words = l.split(" ")
    if len(words) > 6:
        return False

    # Also each band should contain at least one real letter
    if re.match("[a-zA-Z]", l) is None:
        return False
    return True


def read_body(body: str) -> List[str]:
    # Remove double line breaks
    body = remove_double_line_breaks(body)
    lines: List[str] = body.split("\n")

    # Remove empty lines that might still be there for whatever reason
    lines = [l for l in lines if l != ""]

    # Only keep first 5 lines
    lines = lines[:5]
    bands = [clean_up_line(l) for l in lines]
    bands = [b for b in bands if is_band(b)]

    # Remove duplicate entries from malicious voters
    bands = list(set(bands))
    return bands


edges = defaultdict(lambda: defaultdict(int))
band_counts = defaultdict(int)


# It is important to note here, that we will only need to add an edge in one direction, since we basically
# iterate twice over each list
def add_edge(band1: str, band2: str):
    global edges
    edges[band1][band2] += 1


def add_edges_for_bandlist(bands: List[str]):
    global band_counts
    for band in bands:
        band_counts[band] += 1
        for other in bands:
            if band != other:
                add_edge(band, other)


# This is the real way to do it but hits the request limit too quickly
data = requests.get("https://www.reddit.com/r/Metal/comments/8t9trf/shreddits_top_5_of_2018/.json?limit=500",
                    headers={'User-agent': 'Shreddit parser'}).json()

# Use local version to get around request limits
# with open('local.json') as f:
#  data = json.load(f)

# yay for deep nested json
comments = data[1]['data']['children']
comments = clean_comments(comments)

bodies = [c['data']['body'] for c in comments]
band_lists = [read_body(b) for b in bodies]

[add_edges_for_bandlist(l) for l in band_lists]

counts = [{"band": k, "count": v} for k, v in band_counts.items()]
counts = sorted(counts, key=lambda x: x['count'], reverse=True)
print(json.dumps(counts))
print(json.dumps(edges))

with open("counts.json", "w") as f:
    json.dump(counts, f, indent=4)

with open("connections.json", 'w') as f:
    json.dump(edges, f, indent=4)

with open("update.json", 'w') as f:
    json.dump({"last_update": str(datetime.utcnow())}, f, indent=4)


shutil.copy("counts.json", "src/assets/counts.json")
shutil.copy("connections.json", "src/assets/connections.json")
shutil.copy("update.json", "src/assets/update.json")
